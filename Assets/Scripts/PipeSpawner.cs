﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSpawner : MonoBehaviour {

    public float maxYpos;
    public float spawnTime;
    public GameObject pipes;

    // Use this for initialization
    void Start () {
        StartSpawningPipes();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartSpawningPipes() {
        InvokeRepeating("SpawnPipes", .2f, spawnTime);
    }

    public void StopSpawningPipes() {
        CancelInvoke("SpawnPipes");
    }

    void SpawnPipes() {
        Instantiate(pipes, new Vector3(transform.position.x, Random.Range(-maxYpos, maxYpos), 0), Quaternion.identity);
    }
}
