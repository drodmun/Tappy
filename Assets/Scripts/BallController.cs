﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

    Rigidbody2D rb;
    bool started;
    bool gameOver;

    public float upForce;
    
	// Use this for initialization
	void Start () {
        started = false;
        gameOver = false;
        rb = GetComponent<Rigidbody2D>();	
	}
	
	// Update is called once per frame
	void Update () {
        if (!started) {
            if (Input.GetMouseButtonDown(0)) {
                rb.isKinematic = false;
                started = true;
            }
        } else {
            if (!gameOver && Input.GetMouseButtonDown(0)) {
                rb.velocity = Vector2.zero;
                rb.AddForce(new Vector2(0, upForce));
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (!gameOver && other.gameObject.tag == "Pipe") {
            gameOver = true;
        }

        if (!gameOver && other.gameObject.tag == "ScoreChecker") {
            ScoreManager.instance.IncrementScore();
        }
    }
}
