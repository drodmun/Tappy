﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    public static ScoreManager instance;

    public int score;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    // Use this for initialization
    void Start() {
        score = 0;
        PlayerPrefs.SetInt("score", score);
    }

    public void IncrementScore() {
        score++;
    }

    public void StopScore() {
        PlayerPrefs.SetInt("score", score);

        if (PlayerPrefs.HasKey("highScore")) {
            if (score > PlayerPrefs.GetInt("highScore")) {
                PlayerPrefs.SetInt("highScore", score);
            }
        } else {
            PlayerPrefs.SetInt("highScore", score);
        }
    }
}
