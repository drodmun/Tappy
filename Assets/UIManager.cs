﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    public static UIManager instance;
    public Text score;
    public GameObject gameOverPanel;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        score.text = ScoreManager.instance.score.ToString();
    }

    public void GameOver() {
        gameOverPanel.SetActive(true);
    }

    public void Replay() {
        SceneManager.LoadScene("level1");
    }

    public void Menu() {
        SceneManager.LoadScene("Menu");
    }
}
